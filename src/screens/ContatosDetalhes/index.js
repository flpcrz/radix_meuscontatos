import React, { Component } from "react";
import {
    Text,
    Linking,
    Platform,
    View
} from "react-native";
import { Card, Icon } from "react-native-elements";
import ActionButton from "react-native-action-button";
import VectorIcon from "react-native-vector-icons/FontAwesome5";

export default class ContatosDetalhes extends Component {
    static navigationOptions = {
        title: "Contato Detalhes"
    };

    callNumber = phone => {
        const phoneNumber =
            Platform.OS == "android" ? `tel:${phone}` : `telprompt:${phone}`;

        Linking.canOpenURL(phoneNumber)
            .then(supported => {
                if (!supported) {
                    alert("Phone number is not available");
                } else {
                    return Linking.openURL(phoneNumber);
                }
            })
            .catch(err => console.log(err));
    };

    openApp = url => {
        Linking.canOpenURL(url)
            .then(supported => {
                if (!supported) {
                    alert("Could not open app");
                } else {
                    return Linking.openURL(url);
                }
            })
            .catch(err => console.log(err));
    };

    _renderActionButton = (item) => {
        return (
            <ActionButton
                style={{elevation: 45}}
                buttonColor="rgba(231,76,60,1)"
                renderIcon={active =>
                    active ? (
                        <Icon name="plus" type="feather" color="white" />
                    ) : (
                        <Icon name="phone" type="entypo" color="white" />
                    )
                }
            >
                <ActionButton.Item
                    buttonColor="#9b59b6"
                    title="Telefone"
                    onPress={() => this.callNumber(item.telefone)}
                >
                    <Icon name="phone" type="entypo" color="white" />
                </ActionButton.Item>
                <ActionButton.Item
                    buttonColor="#1abc9c"
                    title="Whatsapp"
                    onPress={() => {
                        this.openApp(`whatsapp://send?phone=${item.telefone}`);
                    }}
                >
                    <Icon name="whatsapp" type="font-awesome" color="white" />
                </ActionButton.Item>
                <ActionButton.Item
                    buttonColor="#3498db"                               
                    title="Telegram"
                    onPress={() => {
                        this.openApp(`tg://`);
                    }}
                >
                    <VectorIcon name="telegram-plane" size={20} color="white" />
                </ActionButton.Item>
            </ActionButton>
        );
    };

    render() {
        const item = this.props.navigation.getParam("item");
        return (
            <View style={{ flex: 1 }}> 
            {this._renderActionButton(item)}
                <Card title={item.nome} image={{ uri: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg' }}>
                    <Text style={{ marginBottom: 10 }}>{item.telefone}</Text>
                    <Text style={{ marginBottom: 10 }}>{item.descricao}</Text>
                </Card>
               
            </View>
        );
    }
}
