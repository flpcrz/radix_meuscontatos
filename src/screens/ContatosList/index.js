import React, { Component } from "react";
import {
    TouchableHighlight,
    View,
    FlatList,
    AsyncStorage
} from "react-native";
import { ListItem } from "react-native-elements";
import ActionButton from "react-native-action-button";

class ContatosList extends Component {
    static navigationOptions = {
        title: "Contatos"
    };

    constructor(props){
        super(props)
        this.state = {list: []};
        this.fetchContacts();
    }

    fetchContacts = async () => {
        await AsyncStorage.getItem('novoContato').then((promise) => {
            var list;
            if(promise != null){ 
                 list = JSON.parse(promise)
            }
            else{
                list = []
            }
            this.setState({
                list:list
            })
        }).catch((error) => {
            alert(error);
        })
    }


    removeItem = async (index) => {
        var newList = this.state.list;
        newList.splice(index, 1)

        await AsyncStorage.setItem('novoContato', JSON.stringify(newList)).then(() => {
            this.setState({
                list: newList
            })
        }).catch((error) => {
            alert(error);
        })
    }



    _renderRow = ({ item, index }) => {
        return (
            <ListItem
                roundAvatar
                title={item.nome}
                subtitle={item.descricao}
                avatar={'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg'}
                onPress={() =>
                    this.props.navigation.navigate("ContatosDetalhes", {
                        item: item
                        
                    })
                }
                onLongPress={
                    () => this.removeItem(index)
                }
            />
        );
    };

    render() {
        return (
            <View style={{ flex: 1 }}>
                <FlatList
                    data={this.state.list}
                    renderItem={this._renderRow}
                    keyExtractor={item => item.nome}
                />
                <ActionButton
                    buttonColor="rgba(231,76,60,1)"
                    onPress={() =>
                        this.props.navigation.navigate("ContatosCadastro",{
                            onGoBack: () => this.fetchContacts()
                          })
                    }
                />
            </View>
        );
    }
}

export default ContatosList;
