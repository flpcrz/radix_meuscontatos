import React, { Component } from "react";
import {
    View,
    AsyncStorage
} from "react-native";
import { FormLabel, FormInput, Button } from 'react-native-elements';

class Contato{
    constructor(nome, descricao, telefone){
        this.nome = nome;
        this.descricao = descricao;
        this.telefone = telefone;
    }
}

class ContatosCadastro extends Component {

    constructor (props) {
        super(props)
        this.state ={
            nome: '',
            descricao: '',
            telefone: ''
        }
    }
    static navigationOptions = {
        title: "Adicionar Contato"

        
    };

    _onClick = async () =>{
            await AsyncStorage.getItem('novoContato').then((promise) => {
                debugger;
                var list;
                if(promise != null){
                     list = JSON.parse(promise)
                }
                else{
                    list = []
                }

                list.push(new Contato(this.state.nome, this.state.descricao, this.state.telefone))
                this._onSave(list)
            }).catch((error) => {
                alert(error);
            })
    }

    _onSave = async (list) => {
        await AsyncStorage.setItem('novoContato', JSON.stringify(list)).then(() => {
            this.props.navigation.state.params.onGoBack();
            this.props.navigation.goBack();
        }).catch((error) => {
            alert(error);
        })
    }

    _onBlurNome = (text) => {
        this.setState ({
            nome: text
        })
    }
    _onBlurDescricao = (text) => {
        this.setState({
            descricao: text
        })
    }
    _onBlurTelefone = (text) => {
        this.setState ({
            telefone: text
        })
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <FormLabel>Nome</FormLabel>
                <FormInput onChangeText ={this._onBlurNome}/>

                <FormLabel>Descrição</FormLabel>
                <FormInput onChangeText ={this._onBlurDescricao}/>

                <FormLabel>Telefone</FormLabel>
                <FormInput keyboardType='number-pad' onChangeText ={this._onBlurTelefone}/>

                <Button style={{ marginTop: 36 }}
                    title='BUTTON' 
                    onPress={this._onClick}
                    />
            </View>
        );
    }
}

export default ContatosCadastro;
