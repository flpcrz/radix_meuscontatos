import { createStackNavigator, createAppContainer } from "react-navigation";
import { ContatosList, ContatosDetalhes, ContatosCadastro } from "./screens";

const StackNavigator = createStackNavigator({
    ContatosList,
    ContatosDetalhes,
    ContatosCadastro
});

const Navigation = createAppContainer(StackNavigator);

export default Navigation;
